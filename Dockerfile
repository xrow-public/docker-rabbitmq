FROM rabbitmq:3-management

ENV RABBITMQ_DEFAULT_USER=admin
ENV RABBITMQ_DEFAULT_PASS=centos

RUN chmod -R u+x /usr/local/bin/ && \
    chgrp -R 0 ${RABBITMQ_HOME} && \
    chmod -R g=u ${RABBITMQ_HOME} /etc/passwd

USER 1001
